Created because I had a theory that the Phillies scored more runs early in the game than later. I was wrong.

Written for Python 3 (but will probably run in Python 2). To run, call the script with the 3 letter code of the team and the year interested in. For example, 'py mlb.py phi 2018'. If you're unsure of the 3 letter code for your team, run without providing any values and a current list of team abbreviations will be provided.

Uses MLB GameDay2 for all data. A 'TIMEOUT_VALUE' is available to avoid being throttled. Currently it is set as 1 second. A 'TIMEOUT_VALUE' is also provided to determine how long to wait for a response. Currently it is set at 30 seconds.

All comments, questions, or issues can be addressed to joseph.gruber@gmail.com
