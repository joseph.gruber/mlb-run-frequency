import argparse
import calendar
import datetime
import itertools
import matplotlib.pyplot as plt
import requests
import time
import sys


GD2_URL = 'http://gd2.mlb.com/components/game/mlb/year_{0}/month_{1:02d}/day_{2:02d}/master_scoreboard.json'
TEAMS_URL = 'http://lookup-service-prod.mlb.com/json/named.team_all.bam?sport_code=%27mlb%27&active_sw=%27Y%27&all_star_sw=%27N%27'
TIMEOUT_VALUE = 30
WAIT_VALUE = 1  # Being nice to the MLB GD2 / AWS network


def getMlbTeams():
  try:
    data = requests.get(
        TEAMS_URL, timeout=TIMEOUT_VALUE
    )
    data.raise_for_status()
  except requests.exceptions.RequestException:
    raise RuntimeError(
        'Unable to retrieve the list of teams from MLB.'
    )

  return data.json()


def getScoreboard(day, month, year):
  try:
    data = requests.get(
        GD2_URL.format(year, month, day),
        timeout=TIMEOUT_VALUE)
  except requests.exceptions.RequestException:
    raise RuntimeError(
        'Unable to retrieve the master scoreboard from MLB for{}-{:02d}-{:02d}'.format(
            year, month, day
        )
    )

  if data.status_code != 200:
    return None
  else:
    return data.json()


def teamCodes():
  teams = []

  data = getMlbTeams()

  for team in data['team_all']['queryResults']['row']:
    teams.append(team['name_abbrev'])

  return teams


def isValidYear(year):
  if year >= 1900 and year <= datetime.date.today().year:
    return True

  return False


def games(year, team):
  aggregate_runs = []
  current_year = datetime.date.today().year
  current_month = datetime.date.today().month
  current_day = datetime.date.today().day
  months = list(range(3, 11))  # Hot stove

  for month in months:
    if year == current_year and month > current_month:
      break

    print(
        'Getting data from MLB for {} {}...'.format(
            calendar.month_name[month], year
        )
    )
    days = range(
        1, calendar.monthrange(year, month)[1] + 1
    )

    for day in days:
      if (
          year == current_year
          and month == current_month
          and day > current_day
      ):
        break

      scoreboard = getScoreboard(day, month, year)

      if scoreboard is None:
        continue

      try:
        if type(scoreboard['data']['games']['game']) is list:
          for game in scoreboard['data']['games']['game']:
            if game['game_type'] == 'R' and game['status']['status'] == 'Final':
              if game['home_name_abbrev'] == team:
                aggregate_runs.append(lineScore(game['linescore']['inning'], 'home'))
              elif game['away_name_abbrev'] == team:
                aggregate_runs.append(lineScore(game['linescore']['inning'], 'away'))
        else:
          game = scoreboard['data']['games']['game']
          if game['game_type'] == 'R' and game['status']['status'] == 'Final':
            if game['home_name_abbrev'] == team:
              aggregate_runs.append(lineScore(game['linescore']['inning'], 'home'))
            elif game['away_name_abbrev'] == team:
              aggregate_runs.append(lineScore(game['linescore']['inning'], 'away'))
      except KeyError:
        print(
            'No games found on {}-{}-{}'.format(
                year, month, day
            )
        )

      time.sleep(WAIT_VALUE)

  return aggregate_runs


def lineScore(innings, location):
  runs = []

  for inning in innings:
    try:
      if inning[location]:
        runs.append(int(inning[location]))
    except KeyError:
      pass  # In older games, MLBAM did not provide a value if the bottom of the 9th wasn't played

  return tuple(runs)


def generatePlot(average_runs, team, year):
  innings = range(1, 10)
  _, ax = plt.subplots()

  rects = ax.bar(
      innings,
      average_runs[:9],
      color='#E81828',
      alpha=0.4)
  ax.set_xticks(innings)
  ax.set_title(
      'Average runs per inning for {} in {}'.format(
          team, year
      )
  )
  ax.set_xlabel('Inning')
  ax.set_ylabel('Average runs')

  for rect in rects:
    height = rect.get_height()
    ax.text(
        rect.get_x() + rect.get_width() / 2,
        height,
        str(round(height, 2)),
        ha='center',
        va='bottom',
        fontweight='bold',
        color='#284898'
    )

  plt.show()
  plt.close()


def main(args):
  team_codes = teamCodes()
  team = args.team.upper()
  year = args.year

  if team not in team_codes:
    sys.exit(
        'Invalid MLB team code. Please use one of the following: '
        + ', '.join(team_codes))

  if not isValidYear(year):
    sys.exit(
        'Invalid year provided. Valid values are from 1900 to {}'.format(
            datetime.date.today().year
        )
    )

  print(
      'Gathering data for {} in {}...'.format(
          team, str(year)
      )
  )

  runs = games(year, team)

  total_runs = [
      sum(list(filter(None.__ne__, inning)))
      for inning in itertools.zip_longest(
          *runs, fillvalue=None
      )
  ]

  average_runs = [
      sum(list(filter(None.__ne__, inning)))
      / len(list(filter(None.__ne__, inning)))
      for inning in itertools.zip_longest(
          *runs, fillvalue=None
      )
  ]

  total_games = [len(list(filter(None.__ne__, inning)))
                 for inning in itertools.zip_longest(
      *runs, fillvalue=None
  )
  ]

  print()
  print(
      'Average runs [Total runs/Games] per inning for {} in {}:'.format(
          team, str(year)
      )
  )
  for idx, val in enumerate(average_runs):
    print(
        '{}: {} [{}/{}]'.format(
            idx + 1,
            val,
            total_runs[idx],
            total_games[idx]
        )
    )

  generatePlot(average_runs, team, year)


if __name__ == '__main__':
  parser = argparse.ArgumentParser(
      description='Graph the average runs by inning of a specified MLB team in a specified year.'
  )
  parser.add_argument(
      'team',
      help='three character abbreviation of MLB team',
      type=str
  )
  parser.add_argument(
      'year',
      help='four digit year',
      type=int
  )
  args = parser.parse_args()

  main(args)
